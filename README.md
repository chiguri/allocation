# README #

General Allocation solver for abstract argumentation framework

Codes are written by Sosuke Moriguchi.

## Build

Install OCaml > 4.02 and use "make" command.


## Test

Add "trivialxxx.txt" file written in trivial_graph format into "tests" folder, and then run "make test".
The command generates "results" folder and puts the results there.
