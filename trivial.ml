open Af
open Expression
open Equation
open Trivial_graph

let test_graph = build_graph stdin

let general_allocator = build_equations test_graph

let output_name out_c names n =
  assert (n > 0 && n <= Array.length names); output_string out_c (names.(n))

let output_graph out_c (_, _, names) arr =
  Array.iteri (fun i e ->
                 if i <> 0 then (output_string out_c "\"";
                                 output_name out_c names i;
                                 output_string out_c "\":\n\t";
                                 output_exp out_c e;
                                 output_string out_c "\n\n"))
    arr


let output_result out_c graph arr = (solve_equations (Array.length arr - 1) arr; output_graph out_c graph arr);;

output_result stdout test_graph general_allocator
