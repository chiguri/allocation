
type variable = int

type value = T | F | U

type expression = { mutable exp : exp; mutable subst_n : int; mutable optimized : bool }
and exp =
  | Eval of value
  | Evar of variable
  | Eand of expression * expression
  | Eor of expression * expression
  | Enot of expression


type valuation = variable -> value

val e_val : value -> int -> expression
val e_var : variable -> int -> expression
val e_and : expression -> expression -> int -> expression
val e_or : expression -> expression -> int -> expression
val e_not : expression -> int -> expression

val evaluate : valuation -> expression -> value

val optimize : expression -> unit

(* optimize_not might not be used *)

val get_n_format : variable -> expression -> (expression * expression * expression * expression)

val subst_n : variable -> expression -> expression -> unit

val output_exp : out_channel -> expression -> unit
