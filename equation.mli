open Af
open Expression

val build_equations : graph -> expression array

val solve_equations : int -> expression array -> unit
