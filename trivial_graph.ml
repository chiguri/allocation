open Af

let space_reg = Str.regexp "[ \t\r\n]+"

let build_graph in_c =
  let node_strings =
    let rec heads () =
      let str = List.hd (Str.split space_reg (input_line in_c)) in
        if str = "#" then [] else (str :: heads ()) in
      heads () in
  let num = List.length node_strings in
  let names = Array.of_list ("" :: node_strings) in
  let nodetbl = Hashtbl.create ~random:false (2*num) in
  let _ = Array.iteri (fun i s -> if i <> 0 then Hashtbl.add nodetbl s i) names in
  let edges =
    let rec pairs () =
      let splits =
        try Str.split space_reg (input_line in_c)
        with
            End_of_file -> []
      in
      match splits with
        | [] | _ :: [] -> []
        | n1 :: n2 :: _ -> ((Hashtbl.find nodetbl n1, Hashtbl.find nodetbl n2) :: pairs ()) in
      pairs() in
    (num, edges, names)




