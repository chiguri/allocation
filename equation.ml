open Af
open Expression

let build_equations (n, el, _) =
  let e_val c = e_val c 0 in
  let e_var v = e_var v 0 in
  let e_and e1 e2 = e_and e1 e2 0 in
  let e_not e = e_not e 0 in
  let attacker_list = Array.make (n+1) [] in
  let attacker (n1, n2) = attacker_list.(n2) <- n1 :: attacker_list.(n2) in
  let _ = List.iter attacker el in
  let equation = function
    | [] -> e_val T
    | n0 :: nl -> List.fold_left (fun e n -> e_and (e_not (e_var (-n))) e) (e_not (e_var (-n0))) nl in
  Array.map equation attacker_list



let var_num = ref 0

(* 手抜き *)
let solve_equation num exp =
  let e_val c = e_val c (-num) in
  let e_var v = e_var v (-num) in
  let e_and e1 e2 = e_and e1 e2 (-num) in
  let e_or e1 e2 = e_or e1 e2 (-num) in
  let e_not e = e_not e (-num) in
  let _ = optimize exp in
  let (p, n, c, m) = get_n_format (-num) exp in
  let _ = (optimize p; optimize n; optimize c; optimize m) in
  let e =
    match (p.exp, c.exp) with
      | (Eval F, Eval F) ->
          e_or (e_and (e_val U) (e_and n (e_not m))) m
      | _ ->
          let v = !var_num in
          let _ = (var_num := v + 1) in
          let b = e_var v in
            e_or (e_and p b)
                 (e_or (e_and (e_val U) (e_or n (e_and c b)))
                       m) in
  (optimize e; e)


(* 手抜き実装: どこまで代入したかなどを覚えて代入すると高速化するはず *)

let solve_equations n arr =
  let subst_num num e = Array.iter (fun exp -> subst_n (-num) e exp) arr in
  let iterate i e =
    let exp = solve_equation i e in
    let _ = arr.(i) <- e_var (-i) 0 in
      subst_num i exp in
    Array.iteri iterate arr


