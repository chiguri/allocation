(* This is encoded in UTF-8, but only limited parts *)
(* I write Japanese charactors here to let your editor detect the encoding *)
(* by 森口草介 (Sosuke Moriguchi) *)

type variable = int

type value = T | F | U

type expression = { mutable exp : exp; mutable subst_n : int; mutable optimized : bool }
and exp =
  | Eval of value
  | Evar of variable
  | Eand of expression * expression
  | Eor of expression * expression
  | Enot of expression

let build_e e n b = { exp = e; subst_n = n; optimized = b }
let e_val c n = build_e (Eval c) n true
let e_var v n = build_e (Evar v) n true
let e_and e1 e2 n = build_e (Eand (e1, e2)) n false
let e_or  e1 e2 n = build_e (Eor  (e1, e2)) n false
let e_not e n = build_e (Enot e) n false

type valuation = variable -> value


let evaluate v e =
  let rec eval = function
    | Eval c -> c
    | Evar x -> v x
    | Eand (e1, e2) ->
        begin match (eval e1.exp, eval e2.exp) with
          | (T, T) -> T
          | (F, _) | (_, F) -> F
          | _ -> U
        end
    | Eor (e1, e2) ->
        begin match (eval e1.exp, eval e2.exp) with
          | (F, F) -> F
          | (T, _) | (_, T) -> T
          | _ -> U
        end
    | Enot e ->
        begin match eval e.exp with
          | T -> F | F -> T | U -> U
        end
  in eval e.exp


let rec optimize e =
  if e.optimized then ()
  else begin
    e.optimized <- true;
    match e.exp with
      | Eval _ | Evar _ -> ()
      | Eand (e1, e2) ->
          begin optimize e1;
            match e1.exp with
              | Eval T -> (optimize e2; e.exp <- e2.exp)
              | Eval F -> (e.exp <- Eval F)
              | _ ->
                  begin optimize e2;
                    match e2.exp with
                      | Eval T -> e.exp <- e1.exp
                      | Eval F -> e.exp <- Eval F
                      | _ -> ()
                  end
          end
      | Eor (e1, e2) ->
          begin optimize e1;
            match e1.exp with
              | Eval T -> (e.exp <- Eval T)
              | Eval F -> (optimize e2; e.exp <- e2.exp)
              | _ ->
                  begin optimize e2;
                    match e2.exp with
                      | Eval T -> e.exp <- Eval T
                      | Eval F -> e.exp <- e1.exp
                      | _ -> ()
                  end
          end
      | Enot e1 ->
          begin optimize e1;
            match e1.exp with
              | Eval T -> (e.exp <- Eval F)
              | Eval F -> (e.exp <- Eval T)
              | Eval U -> (e.exp <- Eval U)
              | Enot e2 -> (e.exp <- e2.exp)
              | _ -> ()
          end
  end


(* TODO: implement slower but more agressive simplifier *)
let simplifier = optimize



(* may be very big: at least in Eand case *)
(* building new expression *)
let get_n_format n =
  let e_val c = e_val c n in
  let e_and e1 e2 = e_and e1 e2 n in
  let e_or e1 e2 = e_or e1 e2 n in
  let e_not e = e_not e n in
  let cross_multiply (p1, n1, c1, m1) (p2, n2, c2, m2) =
    (* for Eand case *)
    (e_or (e_and p1 p2)
          (e_or (e_and p1 m2) (e_and p2 m1)),
     e_or (e_and n1 n2)
          (e_or (e_and n1 m2) (e_and n2 m1)),
     e_or (e_and (e_or p1 (e_or n1 m1)) c2)
          (e_or (e_and (e_or p2 (e_or n2 m2)) c1)
                (e_or (e_and c1 c2)
                      (e_or (e_and p1 n2)
                            (e_and n1 p2)))),
     e_and m1 m2) in
  let rec n_format e = match e.exp with
    | Evar n' when n = n' -> (e_val T, e_val F, e_val F, e_val F)
    | Evar _ | Eval _ -> (e_val F, e_val F, e_val F, e)
    | Eand (e1, e2) -> cross_multiply (n_format e1) (n_format e2)
    | Eor (e1, e2) ->
        let (p1, n1, c1, m1) = n_format e1 in
        let (p2, n2, c2, m2) = n_format e2 in
          (e_or p1 p2, e_or n1 n2, e_or c1 c2, e_or m1 m2)
    | Enot e1 -> n_format_not e1
(* Enot is expanded only here *)
  and n_format_not e = match e.exp with
    | Evar n' when n = n' -> (e_val F, e_val T, e_val F, e_val F)
    | Evar _ | Eval _ -> (e_val F, e_val F, e_val F, e_not e)
    | Eand (e1, e2) ->
        let (p1, n1, c1, m1) = n_format_not e1 in
        let (p2, n2, c2, m2) = n_format_not e2 in
          (e_or p1 p2, e_or n1 n2, e_or c1 c2, e_or m1 m2)
    | Eor (e1, e2) -> cross_multiply (n_format_not e1) (n_format_not e2)
    | Enot e1 -> n_format e1
  in n_format


let subst_n n exp =
  let rec subst e =
    if e.subst_n > n then (* This is TOO tightly coupled with order of equation solving *)
      begin
        e.subst_n <- n;
        match e.exp with
          | Evar n' when n = n' -> (e.exp <- exp.exp; e.optimized <- true; true) (* exp itself is not reused? little loss? *)
          | Evar _ | Eval _ -> false
          | Eand (e1, e2) | Eor (e1, e2) ->
              let res1 = subst e1 in
              let res2 = subst e2 in
              let res = res1 || res2 in
                (e.optimized <- (e.optimized && not res); res)
          | Enot e' ->
              let res = subst e' in (e.optimized <- (e.optimized && not res); res)
      end
    else false
  in (optimize exp; fun e -> (ignore (subst e); optimize e))



let value_to_string = function
  | T -> "T" | F -> "F" | U -> "U"


let output_exp out_c exp =
  let rec output n = function
    | Eval v -> output_string out_c (value_to_string v)
    | Evar n -> (output_string out_c "v"; output_string out_c (string_of_int n))
    | Eand (e1, e2) -> (if n > 2 then output_string out_c "("; output 2 e1.exp; output_string out_c "∧"; output 2 e2.exp; if n > 2 then output_string out_c ")")
    | Eor  (e1, e2) -> (if n > 1 then output_string out_c "("; output 1 e1.exp; output_string out_c "∨"; output 1 e2.exp; if n > 1 then output_string out_c ")")
    | Enot e -> (output_string out_c "￢"; output 3 e.exp)
  in output 0 exp.exp

