.PHONY: native
.PHONY: byte
.PHONY: clean
.PHONY: test
.PHONY: small
.PHONY: middle
.PHONY: large



SHELL = /bin/bash

BUILD_FILES := trivial.ml

LIB_FILES := expression.ml equation.ml af.ml trivial_graph.ml
OPTLIBRARIES := $(LIB_FILES:.ml=.cmx)
CLIBRARIES := $(LIB_FILES:.ml=.cmo)


NATIVE_EXES := $(BUILD_FILES:.ml=-native.exe)
BYTE_EXES := $(BUILD_FILES:.ml=-byte.exe)


native: $(NATIVE_EXES)

byte: $(BYTE_EXES)

af.cmi: af.mli
	ocamlc af.mli

expression.cmi: expression.mli
	ocamlc -c expression.mli

equation.cmi: equation.mli af.cmi expression.cmi
	ocamlc -c equation.mli

trivial_graph.cmi: trivial_graph.mli af.cmi
	ocamlc -c trivial_graph.mli


%.cmx: %.ml %.cmi
	ocamlopt -c $<

%.cmo: %.ml %.cmi
	ocamlc -c $<

%-native.exe : %.ml $(OPTLIBRARIES)
	ocamlopt str.cmxa $(OPTLIBRARIES) $< -o $@

%-byte.exe : %.ml $(CLIBRARIES)
	ocamlc str.cma $(CLIBRARIES) $< -o $@



clean:
	rm -rf *.exe *.o *.cmx *.cmi *.cmo results


test: $(NATIVE_EXES)
	mkdir -p results
	@for solver in $(BUILD_FILES:.ml=); do echo "native test for" `ls tests/ | grep $${solver}`; for f in `ls tests/ | grep $${solver}`; do ./$${solver}-native.exe < tests/$${f} > results/$${f/.txt/-native-result.txt} ; done ; done



btest: $(BYTE_EXES)
	mkdir -p results
	@for solver in $(BUILD_FILES:.ml=); do echo "bytecode test for" `ls tests/ | grep $${solver}`; for f in `ls tests/ | grep $${solver}`; do ./$${solver}-byte.exe < tests/$${f} > results/$${f/.txt/-byte-result.txt} ; done ; done


