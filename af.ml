
type node = int (* to be more than zero : 1, 2, ... *)

type edge = node * node

type graph = int * edge list * string array (* (number_of_node, edges, names_of_nodes) *)

